<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BasicController extends Controller
{
    /**
     * @Route("/")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {$places = $this
        ->getDoctrine()
        ->getRepository('AppBundle:Place')
        ->findAll();

        $dishesByPlaces = [];

        foreach($places as $place) {
            $dishes = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Dish')
                ->getMostPopularDishesByPlace($place, 2);

            $dishesByPlaces[] = [
                'place' => $place,
                'dishes' => $dishes
            ];
        }
        dump($dishesByPlaces);

        return $this->render('@App/Basic/index.html.twig', array('dishesByPlaces' => $dishesByPlaces));

    }
}
