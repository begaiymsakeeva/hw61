<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Place;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPlaceData extends Fixture
{
    public const PLACE_ONE = 'Барашек';
    public const PLACE_TWO = 'Obama Bar & Grill';

    public function load(ObjectManager $manager)
    {
        $place1 = new Place();
        $place1
            ->setName('Барашек')
            ->setDescription('Ресторан «Барашек» - культовое место для гурманов нашего города и гостей Бишкека. Мы собрали все самое лучшее, что может отражать истинное радушное гостеприимство: современный интерьер в сочетании дерева и зеркал, располагает к безмятежному и уютному времяпрепровождению; вкусные блюда не только из барашка, а также свежей телятины или охлажденной рыбы; внимательный сервис отличается особенным трепетом и уважением к каждому гостю.')
            ->setImage('barashek.jpeg');

        $manager->persist($place1);

        $place2 = new Place();
        $place2
            ->setName('Obama Bar & Grill')
            ->setDescription('Obama Bar & Grill - ресторан настоящей американской кухни, где царит гостеприимная атмосфера, а изысканная североамериканская и мексиканская кухни и уютный интерьер располагают к приятному времяпровождению.
Мы предлагаем широкий выбор блюд: фирменные бургеры, отменные стейки, кофе Старбакс и ​богатый выбор лучших вин.')
            ->setImage('obama.png');

        $manager->persist($place2);
        $manager->flush();

        $this->addReference(self::PLACE_ONE, $place1);
        $this->addReference(self::PLACE_TWO, $place2);
    }
}
